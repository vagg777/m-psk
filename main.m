function main()
    clear;
    clc;
    close all;
    format short;
    SNR = 0:2:20;
    
    % BER Calculations
    M = 4;
    BER_PSK_4 = PSK(M,"BER");
    BER_PAM_4 = PAM(M,"BER");
    M = 8;
    BER_PSK_8 = PSK(M,"BER");
    BER_PAM_8 = PAM(M,"BER");
    
    % SER Calculations
    M = 4;
    SER_PSK_4 = PSK(M,"SER");
    SER_PAM_4 = PAM(M,"SER");
    M = 8;
    SER_PSK_8 = PSK(M,"SER");
    SER_PAM_8 = PAM(M,"SER");
    
    % Create the BER graph
    figure('Name','BER-SNR figure');
    p1 = plot(SNR, BER_PSK_4, SNR, BER_PSK_8, SNR, BER_PAM_4, SNR, BER_PAM_8);
    xlabel('SNR (dB)');
    ylabel('Bit Error Rate');
    legend([p1(1);p1(2);p1(3);p1(4)],'PSK for M=4','PSK for M=8','PAM for M=4','PAM for M=8','Location','northeast');

    % Create the SER graph
    figure('Name','SER-SNR figure');
    p2 = plot(SNR, SER_PSK_4, SNR, SER_PSK_8, SNR, SER_PAM_4, SNR, SER_PAM_8);
    xlabel('SNR (dB)');
    ylabel('Symbol Error Rate');
    legend([p2(1);p2(2);p2(4);p2(3)],'PSK for M=4','PSK for M=8','PAM for M=4','PAM for M=8','Location','northeast');
end