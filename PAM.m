function [returned_rate] = PAM(M,requested_rate)
%--------------- M-PAM Transmitter [1/3] --------------%

% Data initialisation
gray_encoding = "NO";
if M == 8 && strcmp(requested_rate,"BER") == 1
    gray_encoding = "YES";
end
input_bits = 10000;
bits_sequence = round(rand(input_bits,1)); % (1000x1) array with random values of '0' or '1', uniformly distributed!
 
fprintf("----------------------------------------------------------------------------------------------------------------\n");
fprintf("Running M-PAM simulation for M = %d symbols | mode = %s | Gray Matching: %s\n",M,requested_rate,gray_encoding);

% Mapper (convert blocks of bits -> symbols)
bits_per_symbol = log2(M);
symbols_dict_binary = de2bi(0:M-1,bits_per_symbol); 	% (M-1x3) array that holds binary representations of each 0:M-1 symbol in each row
if M == 8 
    scan_stop = input_bits - 1;
    symbol_numbers_table = zeros(1,3333);
end
if M == 4 
    scan_stop = input_bits;
    symbol_numbers_table = zeros(1,5000);
end
counter = 1; 
for i = 1:bits_per_symbol:scan_stop                              % scan bit_sequence and create bit blocks of log2(M) bits
    bit_block = bits_sequence(i:i+(bits_per_symbol-1));          % bit_sequence (101000100)' -> bit_block (101 | 000 | 100)'
    if strcmp(gray_encoding,"YES") == 1
        symbol_numbers_table(counter) = bi2de(bit_block');       % get the decimal representation of the binary symbol
    else
        symbol_numbers_table(counter) = bi2de(bit_block') + 1;
    end
    counter = counter + 1;                                   
end

% Check whether we must follow Gray Encoding or not
if strcmp(gray_encoding,"YES") == 1 
    symbol_numbers_table = bin2gray(symbol_numbers_table,'pam',M) + 1; % Convert integer symbol_numbers_table to Gray Encoding (array index = 0 is invalid, so add +1)
end

% M-PAM Modulation
s_m = zeros(M,1);   % define the modulation symbol s_m
for m = 0:M-1
    s_m(m+1,:) = 2*m - 1 - M;
end

% Simulation Time Units
fc = 2.5 * 10^6;
Tsymbol = 4 * 10^(-6);
Tsample = 0.1 * 10^(-6);
time_vector = 0:Tsample:Tsymbol-Tsample; % Time vector that has 40 discrete time measurements for the s_m

% Orthogonal Pulse for s_m transmission
g_t = sqrt(2/Tsymbol);

symbols_table = zeros(M,40); 
for i = 1:M
    % each row holds 40 values (for each time measurement = t) of the signal s_m(t). The columns represent each symbol. 
    symbols_table(i,:) = s_m(i,1) * g_t * cos(2*pi*fc*time_vector);
end

s_t = symbols_table(symbol_numbers_table(1),:); % initialise s_t as the 1st row (1st symbol waveform)
for i = 2:length(symbol_numbers_table)
    s_t = [s_t,symbols_table(symbol_numbers_table(i),:)]; % append to s_t as [previous value, next value]
end

%--------------- END OF M-PAM Transmitter[1/3] --------------%

SNR = 0:2:20;
for snr_iteration = 1:length(SNR)
    %--------------- AWGN Channel[2/3] --------------%
    Eb = 1/bits_per_symbol;     % Energy per bit
    variance = Eb/(2 * 10^(SNR(snr_iteration)/10));
    noise = sqrt(variance)*randn(1,length(s_t));
    noise = noise * input_bits;      
    r_t = s_t + noise;          % each row of r_t represents a discrete symbol
    %--------------- END OF AWGN Channel[2/3] --------------%

    %--------------- M-PAM Receiver[3/3] --------------%

    % M-PAM De-modulation (receiver fully synchronised with the transmitter)
    a = g_t * cos(2*pi*fc*time_vector);         
    r1 = zeros(length(symbol_numbers_table),1);
    r_index = 1;
    for i = 1:40:length(r_t)
        b = r_t(i:i+39);      % scan each 40 values of r_t -> scan every discrete symbol
        r1(r_index) = (a*b')/(a*a');    
        r_index = r_index + 1;
    end

    % Decision Device
    estimated_symbols_table = zeros(length(symbol_numbers_table),1);
    distances = zeros(length(symbol_numbers_table),M);
    for i = 1:length(symbol_numbers_table)
        r = r1(i);
        for m = 1:M
            distances(i,m) = norm( (s_m(m,:)-r), 2);            % find the norm-2 distance of r=[r1,r2] towards all symbols
        end
        [~,estimated_symbols_table(i)] = min(distances(i,:));   % find symbol with smallest distance, so which symbol was sent
    end 

    % Demapper
    if strcmp(gray_encoding,"YES") == 1
        estimated_symbols_table = estimated_symbols_table - 1;                      % return from Gray Encoding (gray2bin accepts values in [O,M-1], so -1 for all columns
        estimated_binary_blocks = gray2bin(estimated_symbols_table,'pam',M);        % convert from Gray to integer (decimal)
        estimated_binary_blocks = de2bi(estimated_binary_blocks',bits_per_symbol);  % convert from decimal to binary
        estimated_symbols_table = estimated_symbols_table + 1;                      % add + 1 in all symbol values since we subtracted -1 before
    else
        estimated_binary_blocks = symbols_dict_binary(estimated_symbols_table,:);   % convert estimated symbols to blocks of binary representations
    end

    estimated_sequence = estimated_binary_blocks(1,:);   % estimated sequence must be an horizontal stream, not blocks of data, so data must be appended horizontally
    for i = 2:length(estimated_binary_blocks)
        estimated_sequence = [estimated_sequence,estimated_binary_blocks(i,:)]; 
    end
    %--------------- END OF M-PAM Receiver[3/3] --------------%

    % BER and SER Calculation for each SNR iteration using MATLAB built-in functions
    [~,BER(snr_iteration)] = biterr(estimated_sequence',bits_sequence(1:length(estimated_sequence)));
    [~,SER(snr_iteration)] = symerr(estimated_symbols_table,symbol_numbers_table');
end

if strcmp(requested_rate,"BER") == 1
    returned_rate = BER;
else
    returned_rate = SER;
end

end